package at.itkolleg;


public class Film extends Medium {
    private int spielzeit;
    private Qualitaet bildquali;


    public Film(String titel, String kommentar, int jahr, int monat, int tag, String genreName, int spielzeit, Qualitaet bildquali,Urheber urheber) {
        super(titel, kommentar, jahr, monat, tag, genreName,urheber);
        this.spielzeit = spielzeit;
        this.bildquali = bildquali;

    }

    public int getSpielzeit() {
        return spielzeit;
    }

    public void setSpielzeit(int spielzeit) {
        this.spielzeit = spielzeit;
    }

    public Qualitaet getBildquali() {
        return bildquali;
    }

    public void setBildquali(Qualitaet bildquali) {
        this.bildquali = bildquali;
    }

    public void anzeigen(){
        super.anzeigen();
        System.out.println(" Spielzeit: "+getSpielzeit()+" Bildquali: "+ getBildquali()+" Regisseur "+getUrheber());
    }
}
