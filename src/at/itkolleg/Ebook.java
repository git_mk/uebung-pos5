package at.itkolleg;

public class Ebook extends Medium {
    private String iSBN;
    private int seitenanzahl;
    private String auflage;


    public Ebook(String titel, String kommentar, int jahr, int monat, int tag, String genreName, String iSBN, int seitenanzahl, String auflage,Urheber urheber) {
        super(titel, kommentar, jahr, monat, tag, genreName, urheber);
        this.iSBN = iSBN;
        this.seitenanzahl = seitenanzahl;
        this.auflage = auflage;

    }

    public String getiSBN() {
        return iSBN;
    }

    public void setiSBN(String iSBN) {
        this.iSBN = iSBN;
    }

    public int getSeitenanzahl() {
        return seitenanzahl;
    }

    public void setSeitenanzahl(int seitenanzahl) {
        this.seitenanzahl = seitenanzahl;
    }

    public String getAuflage() {
        return auflage;
    }

    public void setAuflage(String auflage) {
        this.auflage = auflage;
    }

    public void anzeigen(){
        super.anzeigen();
        System.out.println(" ISBN: "+getiSBN()+" Seitenanzahl: "+getSeitenanzahl()+" Auflage: "+getAuflage()+" Autor: "+getUrheber());
    }
}
