package at.itkolleg;

import java.util.ArrayList;

public class Medienbibliothek {
    private ArrayList<Medium> medienbib;


    public Medienbibliothek() {
        this.medienbib = new ArrayList<>();
    }

    public void mediumHinzufuegen(Medium m) {
        if (m != null) {
            medienbib.add(m);
        }
    }
    public  void listeAusgeben(){
        for(Medium m:medienbib){
            m.anzeigen();
        }
    }
    public void titelSuche(String name){
        for(int index=0;index<medienbib.size();index++){
            if(medienbib.get(index).getTitel().contains(name))
                medienbib.get(index).anzeigen();

        }
    }

}