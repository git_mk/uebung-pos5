package at.itkolleg;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public abstract class Medium {
    private String titel;
    private String kommentar;
    private Genre genre;
    private String  datum;
    private Urheber urheber;


    public Medium(String titel, String kommentar, int jahr,int monat,int tag, String genreName, Urheber urheber) {
        this.titel = titel;
        this.kommentar = kommentar;
        this.genre=new Genre(genreName);
        this.urheber=urheber;
        setDatum(jahr,monat,tag);

    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(int jahr, int monat, int tag) {
        monat=monat-1;
        GregorianCalendar erscheinungsdatum = new GregorianCalendar(jahr, monat, tag);
        SimpleDateFormat datumsformatierer=new SimpleDateFormat("dd. MMMM yyyy");
        this.datum=datumsformatierer.format(erscheinungsdatum.getTime());
    }

    public Urheber getUrheber() {
        return urheber;
    }

    public void anzeigen(){
        System.out.println("Titel: "+getTitel()+" Kommentar: "+getKommentar()+" Erscheinungsdatum: "+ getDatum() +" Genre: "+genre.getName());
    }
}
